/*

//menampilkan pesan peringatan atau infrmasi
alert('Hello World')

//menampilkan teks ke console
console.log('Hello World')
console.error('This is an error')
console.warn('This is a warning')

//Variable

    //deklarasi variable let dengan nilai tertentu
    let age = 30;
    //deklarasi nilai dari variable age yang baru (nilai let variable dapat berganti-ganti)
    age = 31;

    //menampilkan isi dari variable age
    console.log(age);

    //deklarasi variable const dengan nilai tertentu
    const nama ="Agus";
    //menampilkan isi dari variable nama
    console.log(nama);

//Strings
    
    //deklarasi variable s dengan nilai berupa string
    const s = 'Hello World';
    //menampilkan nilai dari panjang variable s
    console.log(s.length);
    //menampilkan hasil dari konversi nilai variable s menjadi uppercase
    console.log(s.toUpperCase());
    //menampilkan hasil dari konversi nilai variable s menjadi lowercase
    console.log(s.toLowerCase());
    //menampilkan nilai dari variable s dari hanya dari indeks ke 0 sampai 5
    console.log(s.substring(0, 5));
    //menampilkan nilai dari variable s yang telah displit menjadi elemen-elemen array
    console.log(s.split(''));

//Tipe Data

    //deklarasi variable rating yang memiliki tipe data number(double)
    const rating = 4.5;
    //deklarasi variable isChecked yang memiliki tipe data boolean
    const isChecked = true;
    //deklarasi variable umur yang memiliki tipe data number
    const umur = 20;
    //deklarasi variable x yang memiliki tipe data null (object)
    const x = null;

    //menampilkan tipe data dari variable
    console.log(typeof rating);
    console.log(typeof isChecked);
    console.log(typeof x);

    //menampilkan teks dan nilai dari variable nama dan umur
    console.log('My Name is ' + nama + ' and I am ' + umur);
    //deklarasi variable hello yang berisi template string (menggabungkan teks dan variable lebih rapi)
    const hello = `My name is ${nama} and I am ${umur}`;
    //menampilkan isi dari variable hello
    console.log(hello);

//Array

    //deklarasi variable numbers sebagai array dengan 5 elemen
    const numbers = new Array(1,2,3,4,5);
    //menampilkan isi dari variable numbers
    console.log(numbers);

    //deklarasi variable fruits sebagai array 
    const fruits = ['Apples', 'Oranges','Pears'];
    //menambahkan elemen pada indeks ke3 array fruits dengan nilai tertentu
    fruits[3] = 'Grapes';

    //memasukkan value ke elemen array fruits paling akhir
    fruits.push('mangos');
    //memasukkan value ke elemen array fruits paling kiri
    fruits.unshift('Strawberries');
    //menghapus isi array paling akhir
    fruits.pop();

    //menampilkan nilai apakah array fruits adalah sebuah array
    console.log(Array.isArray(fruits));

    //menampilkan elemen indeks ke-1 dari array fruits
    console.log(fruits[1]);
    //menampilkan seluruh isi dari array
    console.log(fruits);
    //menampilkan indeks ke berapa value 'Oranges'
    console.log(fruits.indexOf('Oranges'));

    //membuat const variable dengan object literals
    const person = {
        //membuat properti firstName dengan value berupa string
        firstName: 'John',
        //membuat properti lastName dengan value berupa string
        lastName: 'Doe',
        //membuat properti age dengan value berupa number
        age: 30,
        //membuat properti hobbies dengan value berupa array
        hobbies: ['music', 'movies', 'sports'],
        //membuat properti address
        address: {
            //membuat properti street pada properti addres dengan value berupa string
            street:'50 min st',
            //membuat properti city pada properti addres dengan value berupa string
            city: 'Boston',
            //membuat properti state pada properti addres dengan value berupa string
            state: 'MA'
        }
    }

    //menampilkan value dari properti firstName
    console.log(person.firstName);
    //menampilkan value dari properti hobbies pada indeks ke-1
    console.log(person.hobbies[1]);

    //mengubah properti menjadi variable constant
    const {firstName, lastName, address: {city}} = person;

    //menampilkan value dari variable city
    console.log(city);

    //menambahkan properti email pada variabel person dengan value tertentu
    person.email = "john@gmail.com";

    //menampilkan semua isi dari person
    console.log(person);

    //deklarasi variable todos menjadi sebuah array
    const todos = [
        {
            //membuat properti id dengan value berupa number
            id: 1,
            //membuat properti text dengan value berupa string
            text: 'Take out trash',
            //membuat properti isCompleted dengan value berupa boolean
            isCompleted: true
        },
        {
            //membuat properti id dengan value berupa number
            id: 2,
            //membuat properti text dengan value berupa string
            text: 'Meeting with boss',
            //membuat properti isCompleted dengan value berupa boolean
            isCompleted: true
        },
        {
            //membuat properti id dengan value berupa number
            id: 3,
            //membuat properti text dengan value berupa string
            text: 'Dentist appt',
            //membuat properti isCompleted dengan value berupa boolean
            isCompleted: false
        }
    ];

    //menampilkan value dari properti text pada array todos indeks ke-1
    console.log(todos[1].text);

    //deklarasi variabel todoJSON yang mengubah array todos menjadi json format
    const todoJSON = JSON.stringify(todos);
    //menampilkan variabel todoJSON
    console.log(todoJSON);
    
//Looping

    //FOR
        //membuat looping for dengan nilai awal i=0 dan kondisi berhenti jika i <2 
        for(let i = 0; i<2; i++){
            //menampilkan hasil dari looping
            console.log(`For Loop Number: ${i}`);
        }

        //membuat looping for dengan nilai awal i=0 dan kondisi berhenti i < panjang array todos
        for(let i=0; i < todos.length; i++){
            //menampilkan properti text pada setiap indeks yang ada pada array todos
            console.log(todos[i].text);
        }

        //membuat looping for dengan variable todo (elemen pada todos) pada array todos
        for(let todo of todos){
            //menampilkan properti tex untuk setiap element todo
            console.log(todo.text);
        }

    //WHILE
        //deklarasi variable i=0
        let i=0;
        //membuat looping while dengan kondisi berhenti jika i < 2
        while(i< 2){
            //menampilkan hasil dari looping
            console.log(`While Loop Number: ${i}`);
            //increment untuk variable i
            i++;
        }

    //forEach
        //membuat looping forEach dari array todos dengan parameter todo
        todos.forEach(function(todo){
            //menampilkan hasil looping properti text pada setiap elemen
            console.log(todo.text);
        });

    //deklarasi variable todoText dengan map (mengolah array dan menghasilkan object baru)
    const todoText = todos.map(function(todo){
        //mengembalikan nilai properti text
        return todo.text;
    });
    //menampilkan isi dari todoText
    console.log(todoText);

    //deklarasi variable todoCompleted dengan filter (menyaring element yang diinginkan)
    const todoCompleted = todos.filter(function(todo){
        //mengembalikan nilai properti isCompleted yang bernilai true
        return todo.isCompleted === true;
    });
    //menampilkan isi dari todoCompleted
    console.log(todoCompleted);

//If Else
    //membuat percabangan if else dengan kondisi pertama
    if(x === 10) {
        //menampilkan kalimat pernyataan jika kondisi 1 terpenuhi / benar
        console.log('x is 10');
    //menjalankan kondisi ke 2 jika kondisi pertama tidak terpenuhi
    }else if(x > 10){
        //menampilkan kalimat pernyataan jika kondisi 1 terpenuhi / benar
        console.log('x is greater than 10')
    //menjalankan kondisi terakhir jika kondisi pertama dan kedua tidak terpenuhi
    }else {
        //menampilkan kalimat pernyataan jika kondisi 1 terpenuhi / benar
        console.log('x is less than 10');
    }

//Switch 
    //deklarasi variable color dengan value berupa string
    const color = 'green';
    //membuat percabangan switch case dengan variable color
    switch(color){
        //menjalankan case jika kondisi variable color sama dengan red
        case 'red':
            //menampilkan kalimat pernyataan yang sesuai
            console.log('color is red');
            //menghentikan perulangan
            break;
        //menjalankan case jika kondisi variable color sama dengan blue
        case 'blue':
            //menampilkan kalimat pernyataan yang sesuai
            console.log('color is blue');
            //menghentikan perulangan
            break;
        //menjalankan case jika tidak ada case yang sesuai
        default:
            //menampilkan kalimat pernyataan yang sesuai
            console.log('color is NOT red or blue');
            //menghentikan perulangan
            break;
    }

//Function
    //membuat function addNums yang memiliki 2 parameter default
    function addNums(num1 = 1, num2 = 2){
        //mengembalikan nilai dengan menjumlahkan kedua parameter
        return num1 + num2;
    }
    //menampilkan hasil dari pemanggilan fungsi dengan parameter tertentu
    console.log(addNums(5,5));


    //membuat constructor function dengan 3 parameter
    function Person (firstName, lastName, dob) {
        //membuat constructor terhadap variable firstName
        this.firstName = firstName;
        //membuat constructor terhadap variable lastName
        this.lastName = lastName;
        //membuat constructor terhadap variable dob yang diformat menjadi tipe data Date
        this.dob = new Date(dob);

    
        // this.getBirthYear= function(){
        //     return this.dob.getFullYear();
        // }
        // this.getFullName = function(){
        //     return `${this.firstName} ${this.lastName}`;
        // }      
    }

    //membuat object prototype dari constructor person untuk mngambil tahun lahir
    Person.prototype.getBirthYear = function(){
        //mengembalikan nilai dengan method getFullYear
        return this.dob.getFullYear();
    }

    //membuat object prototype dari constructor person untuk mngambil nama lengkap
    Person.prototype.getFullName = function(){
         //mengembalikan nilai dengan template string
        return `${this.firstName} ${this.lastName}`;

    }

//Instantiate object

    //deklarasi variable person1 sebagai objek baru dari Person
    const person1 = new Person('John', 'Doe','4-3-1980');

    //menampilkan isi dari person1
    console.log(person1);
    //menampilkan tahun lahir dari person1
    console.log(person1.dob.getFullYear());

//Single Element Selectors
    //menampilkan element berdasarkan id yang ada pada my-form
    console.log(document.getElementById('my-form'));
    //menampilkan element pertama dari .container yang dipilih menggunakan querySelector
    console.log(document.querySelector('.container'));

//Multiple Element Selectors
    //menampilkan semua element yang dipilih dari .item menggunakan querySelectorAll
    console.log(document.querySelectorAll('.item'));
    //menampilkan koleksi berdasarkan nama tag li
    console.log(document.getElementsByTagName('li'));
    //menampilkan koleksi berdasarkan nama class item
    console.log(document.getElementsByClassName('item'));

//deklarasi variable item yang dipilih dari .item
const items = document.querySelectorAll('.item');
//menampilkan isi dari variable items menggunakan perulangan forEach
items.forEach((item) => console.log(item));

 

//Manipulate DOM

    //deklarasi variable ul untuk mengambil elemen yang dipilih dari .items
    const ul = document.querySelector('.items');

    //menghapus element ul
    // ul.remove();

    //menghapus element terakhir pada ul
    // ul.lastElementChild.remove();

    //mengganti element pertama ul dengan value tertentu menggunakan textContent 
    ul.firstElementChild.textContent = 'Hello';
    //mengganti element pada indeks ke-1 dengan value tertentu menggunakan innerText
    ul.children[1].innerText = 'Brad';
    //mengganti element terakhir ul dengan value teks html menggunakan innerHTML
    ul.lastElementChild.innerHTML = '<h4>Hello</h4>';

 
//Events

    //deklarasi variable btn yang dipilih dari .btn menggunakan querySelector
    const btn = document.querySelector('.btn');
    //mengubah style background dari variable btn dengan value warna tertentu
    btn.style.background = 'red';

    //Mouse events

    //menambahkan event listener pada saat mengklik button
    btn.addEventListener('click', (e) => {
        
        //mencegah terjadinya event bawaan
        e.preventDefault();

        //menampilkan target class
        console.log(e.target.className);
        //mengubah warna background pada element id my-form
        document.getElementById('my-form').style.background = '#ccc';
        //mengubah warna background pada element class body
        document.querySelector('body').classList.add('bg-dark')
    });

    //keyboard event
    //deklarasi variable nameInput menggunakan querySelector pada id name
    const nameInput = document.querySelector('#name');

    //menambahkan event listener jika berhasil menginput sesuatu
    nameInput.addEventListener('input', e => {
        //menambahkan nilai dari element name yang diinputkan pada .container
        document.querySelector('.container').append(nameInput.value);
    });

   */
//Input Form Script

    // Put DOM elements into variables

    //deklarasi variable myForm dari element dengan id my-form
    const myForm = document.querySelector('#my-form');
    //deklarasi variable nameInputForm dari element dengan id name
    const nameInputForm = document.querySelector('#name');
    //deklarasi variable emailInput dari element dengan id email
    const emailInput = document.querySelector('#email');
    //deklarasi variable msg dari element claass msg
    const msg = document.querySelector('.msg');
    //deklarasi variable userList dari element dengan id users
    const userList = document.querySelector('#users');

    // Listen for form submit

    //menambahkan event listener ketika akan mengirimkan elemen elemen input jika input dengan type submit ditekan
    myForm.addEventListener('submit', onSubmit);

    //membuat function onsubmit dengan parameter e
    function onSubmit(e) {

    //mencegah terjadinya event bawaan
    e.preventDefault();
    
        //membuat percabangan jika salah satu input merupakan string kosong
        if(nameInputForm.value === '' || emailInput.value === '') {

            //memberikan notifikasi atau peringatan
            //alert('Please enter all fields');

            //menambahkan classlist error
            msg.classList.add('error');
            //memberikan pesan error
            msg.innerHTML = 'Please enter all fields';

            //menghilangkan pesan error setelah 3 detik
            setTimeout(() => msg.remove(), 3000);

        //jika kondisi pertama tidak terpenuhi    
        } else {
            //deklarasi variabel li untuk membuat list item yang baru
            const li = document.createElement('li');

            //menambahkan node teks dari nilai yang diinputkan
            li.appendChild(document.createTextNode(`${nameInputForm.value}: ${emailInput.value}`));

            //menambahkan html pada tampilan teks
            li.innerHTML = `<strong>${nameInputForm.value}</strong>: ${emailInput.value}`;

            //menambahkan element li pada userlist
            userList.appendChild(li);

            //membersihkan field name input
            nameInputForm.value = '';
            //membersihkan field email input
            emailInput.value = '';
        }
    }


